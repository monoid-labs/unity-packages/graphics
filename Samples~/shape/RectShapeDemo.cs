﻿using Unity.Mathematics;
using UnityEngine;

namespace Monoid.Unity.Graphics.Samples {

  using Mesh = UnityEngine.Mesh;

  [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
  public class RectShapeDemo : MonoBehaviour {
    public float2 extent = math.float2(1, 1);
    public int2 slices;

    void Refresh() {
      var meshFilter = GetComponent<MeshFilter>();
      var mesh = meshFilter.sharedMesh;
      if (!mesh) {
        meshFilter.sharedMesh = mesh = new Mesh();
        mesh.name = name;
      }
      slices = math.max(0, slices);
      mesh.Set(Create.Rect(2.0f * extent.x, 2.0f * extent.y, slices.x, slices.y));
    }

    #region Unity Messages

    void Awake() => Refresh();

    void OnValidate() => Refresh();

    #endregion
  }

}