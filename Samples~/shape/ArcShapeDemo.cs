﻿using Unity.Mathematics;
using UnityEngine;

namespace Monoid.Unity.Graphics.Samples {

  using Mesh = UnityEngine.Mesh;

  [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
  public class ArcShapeDemo : MonoBehaviour {
    public float2 radius = math.float2(0, 1);
    public float2 angle = math.float2((float)-math.PI, (float)math.PI);
    public int2 slices = math.int2(8, 0);

    void Refresh() {
      var meshFilter = GetComponent<MeshFilter>();
      var mesh = meshFilter.sharedMesh;
      if (!mesh) {
        meshFilter.sharedMesh = mesh = new Mesh();
        mesh.name = name;
      }
      slices = math.max(0, slices);
      mesh.Set(Create.DiscSector(radius.x, radius.y, math.abs(angle.y - angle.x), 0.5f * (angle.x + angle.y), slices.x, slices.y));
    }

    #region Unity Messages

    void Awake() => Refresh();

    void OnValidate() => Refresh();

    #endregion
  }

}