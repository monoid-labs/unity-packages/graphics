﻿using Unity.Mathematics;
using UnityEngine;

namespace Monoid.Unity.Graphics.Samples {

  using Mesh = UnityEngine.Mesh;

  [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
  public class SphereShapeDemo : MonoBehaviour {
    public float radius = 1;
    public int2 slices = math.int2(8, 8);

    void Refresh() {
      var meshFilter = GetComponent<MeshFilter>();
      var mesh = meshFilter.sharedMesh;
      if (!mesh) {
        meshFilter.sharedMesh = mesh = new Mesh();
        mesh.name = name;
      }
      slices = math.max(0, slices);
      mesh.Set(Create.Sphere(radius, slices.x, slices.y));
    }

    #region Unity Messages

    void Awake() => Refresh();

    void OnValidate() => Refresh();

    #endregion
  }

}