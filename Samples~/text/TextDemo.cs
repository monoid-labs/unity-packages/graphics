﻿using System;
using UnityEngine;

namespace Monoid.Unity.Graphics.Samples {

  using Mesh = UnityEngine.Mesh;

  [ExecuteInEditMode]
  public class TextDemo : MonoBehaviour {

    [Serializable]
    public struct Settings {
      public Color color;
      public TextAnchor textAnchor;
      public Font font;
      public FontStyle fontStyle;
      public int fontSize;
      public float scaleFactor;
      public bool richText;
    }

    public string text;
    public Settings settings;

    Mesh mesh;


    void Refresh() {
      if (!mesh) {
        // meshFilter.sharedMesh = mesh = new Mesh();
        mesh = new Mesh();
        mesh.name = name;
      }

      var config = new TextGenerationSettings {
        color = settings.color,
        textAnchor = settings.textAnchor,
        pivot = TextMeshGenerator.AnchorPivot(settings.textAnchor),
        font = settings.font,
        fontStyle = settings.fontStyle,
        fontSize = settings.fontSize,
        scaleFactor = settings.scaleFactor,
        richText = settings.richText,
        verticalOverflow = VerticalWrapMode.Overflow,
        horizontalOverflow = HorizontalWrapMode.Overflow,
        generateOutOfBounds = true,
        updateBounds = true,
      };

      mesh.Set(text, in config);
      UnityEngine.Graphics.DrawMesh(mesh, transform.localToWorldMatrix, config.font.material, 0);

    }

    #region Unity Messages

    void LateUpdate() => Refresh();

    #endregion
  }

}
