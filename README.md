# Scripts related to Graphics for Unity3D

Shape & Image generation and more.

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))
