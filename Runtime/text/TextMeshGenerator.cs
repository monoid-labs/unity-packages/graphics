using System.Collections.Generic;
using UnityEngine;

namespace Monoid.Unity.Graphics {

  public static class TextMeshGenerator {

    public sealed class Cache {
      readonly TextGenerator generator = new TextGenerator();
      readonly List<UIVertex> uiVertices;
      readonly List<Vector3> positions, normals;
      readonly List<Vector4> tangents;
      readonly List<Vector2> textures;
      readonly List<Color32> colors;
      readonly List<int> indices;

      public Cache() {
        uiVertices = new List<UIVertex>();
        positions = new List<Vector3>();
        normals = new List<Vector3>();
        tangents = new List<Vector4>();
        textures = new List<Vector2>();
        colors = new List<Color32>();
        indices = new List<int>();
      }

      public Cache(int vertices) {
        uiVertices = new List<UIVertex>(vertices);
        positions = new List<Vector3>(vertices);
        normals = new List<Vector3>(vertices);
        tangents = new List<Vector4>(vertices);
        textures = new List<Vector2>(vertices);
        colors = new List<Color32>(vertices);
        indices = new List<int>(vertices * 6 / 4);
      }

      public Rect Extents => generator.rectExtents;

      public void CopyFrom(string text, in TextGenerationSettings settings) {
        generator.Invalidate();
#if UNITY_EDITOR
        generator.PopulateWithErrors(text, settings, null);
#else
        generator.Populate(text, settings);
#endif

        uiVertices.Clear();

        int vertices = generator.vertexCount;
        Clear(uiVertices, vertices);
        Clear(positions, vertices);
        Clear(normals, vertices);
        Clear(tangents, vertices);
        Clear(textures, vertices);
        Clear(colors, vertices);

        generator.GetVertices(uiVertices);
        foreach(var uiVertex in uiVertices) {
          positions.Add(uiVertex.position);
          normals.Add(uiVertex.normal);
          tangents.Add(uiVertex.tangent);
          textures.Add(uiVertex.uv0);
          colors.Add(uiVertex.color);
        }

        int quads = vertices /  4;
        Clear(indices, quads * 6);
        for(int i = 0; i<quads; i++) {
          int v = 4 * i;
          indices.Add(v);
          indices.Add(v + 1);
          indices.Add(v + 2);
          indices.Add(v);
          indices.Add(v + 2);
          indices.Add(v + 3);
        }
      }

      public void CopyTo(Mesh mesh) {
        mesh.Clear();
        mesh.SetVertices(positions);
        mesh.SetNormals(normals);
        mesh.SetTangents(tangents);
        mesh.SetUVs(0, textures);
        mesh.SetColors(colors);
        mesh.SetTriangles(indices, 0, true);
      }


      static void Clear<T>(List<T> list, int n) {
        list.Clear();
        list.Capacity = Mathf.Max(list.Capacity, n);
      }
    }

    static readonly Cache DefaultCache = new Cache();

    public static Rect Set(this Mesh mesh, string text, in TextGenerationSettings settings) => Set(mesh, text, in settings, DefaultCache);

    public static Rect Set(this Mesh mesh, string text, in TextGenerationSettings settings, Cache cache) {
      cache.CopyFrom(text, in settings);
      cache.CopyTo(mesh);
      return cache.Extents;
    }

    static readonly Vector2[] anchorPivots;

    static TextMeshGenerator() {
      anchorPivots = new Vector2[9];
      anchorPivots[(int) TextAnchor.UpperLeft]    = new Vector2(0.0f, 1.0f);
      anchorPivots[(int) TextAnchor.UpperCenter]  = new Vector2(0.5f, 1.0f);
      anchorPivots[(int) TextAnchor.UpperRight]   = new Vector2(1.0f, 1.0f);
      anchorPivots[(int) TextAnchor.MiddleLeft]   = new Vector2(0.0f, 0.5f);
      anchorPivots[(int) TextAnchor.MiddleCenter] = new Vector2(0.5f, 0.5f);
      anchorPivots[(int) TextAnchor.MiddleRight]  = new Vector2(1.0f, 0.5f);
      anchorPivots[(int) TextAnchor.LowerLeft]    = new Vector2(0.0f, 0.0f);
      anchorPivots[(int) TextAnchor.LowerCenter]  = new Vector2(0.5f, 0.0f);
      anchorPivots[(int) TextAnchor.LowerRight]   = new Vector2(1.0f, 0.0f);
    }

    public static Vector2 AnchorPivot(TextAnchor anchor) => anchorPivots[(int) anchor];

  }

}
