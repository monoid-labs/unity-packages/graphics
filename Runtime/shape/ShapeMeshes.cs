using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace Monoid.Unity.Graphics {

  using Mesh = UnityEngine.Mesh;

  public static class ShapeMeshes {

    #region Cache

    public sealed class Cache {
      public readonly List<Vector3> vertices = new List<Vector3>();
      public readonly List<Vector3> normals = new List<Vector3>();
      public readonly List<Vector2> uvs = new List<Vector2>();
      public readonly List<int> triangles = new List<int>();

      public void CopyTo(Mesh mesh) {
        mesh.Clear();
        mesh.SetVertices(vertices);
        mesh.SetNormals(normals);
        mesh.SetUVs(0, uvs);
        mesh.SetTriangles(triangles, 0, true);
      }

      public void CopyFrom<T>(T shape) where T : IShape {
        int n;

        n = shape.Vertices;
        Clear(vertices, n);
        Clear(normals, n);
        Clear(uvs, n);
        for (int i = 0; i < n; i++) {
          vertices.Add(shape.Position(i));
          normals.Add(shape.Normal(i));
          uvs.Add(shape.Texture(i));
        }

        n = shape.Triangles;
        Clear(triangles, n);
        for (int i = 0; i < n; i++) {
          var tri = shape.Triangle(i);
          triangles.Add(tri.x);
          triangles.Add(tri.y);
          triangles.Add(tri.z);
        }
      }

      static void Clear<T>(List<T> list, int n) {
        list.Clear();
        list.Capacity = math.max(list.Capacity, n);
      }
    }

    #endregion

    static readonly Cache DefaultCache = new Cache();

    public static void Set<T>(this Mesh mesh, T shape) where T : IShape => Set(mesh, shape, DefaultCache);

    public static void Set<T>(this Mesh mesh, T shape, Cache cache) where T : IShape {
      cache.CopyFrom(shape);
      cache.CopyTo(mesh);
    }
  }

}
