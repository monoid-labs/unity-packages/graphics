using Unity.Mathematics;

namespace Monoid.Unity.Graphics {

  public struct TransformedShape<T> : IShape where T : IShape {

    readonly T shape;
    readonly float4x4 transform;
    readonly float3x3 transformIT;

    public TransformedShape (in T s, float4x4 m) {
      shape = s;
      transform = m;
      transformIT = math.transpose (math.inverse (math.float3x3 (m[0].xyz, m[1].xyz, m[2].xyz)));
    }

    public int Vertices => shape.Vertices;
    public int Triangles => shape.Triangles;

    public int3 Triangle (int index) => shape.Triangle (index);

    public float3 Position (int index) {
      var p = math.float4 (shape.Position (index), 1);
      p = math.mul (transform, p);
      return p.xyz / p.w;
    }

    public float3 Normal (int index) => math.mul (transformIT, shape.Normal (index));

    public float2 Texture (int index) => shape.Texture (index);
  }

}
