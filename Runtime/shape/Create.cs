using Unity.Mathematics;

namespace Monoid.Unity.Graphics {

  public static partial class Create {

    public static TransformedShape<T> Transform<T>(in T shape, float4x4 transform) where T : IShape {
      return new TransformedShape<T>(in shape, transform);
    }

    public static RectShape Rect(float x, float y, int xSlices = 0, int ySlices = 0) {
      return new RectShape { extent = 0.5f * math.float2(x, y), slices = math.int2(xSlices, ySlices) };
    }


    public static ArcShape Circle(float radius, int segmentSlices = 32, int loopSlices = 0) {
      return Disc(0.0f, radius, segmentSlices, loopSlices);
    }

    public static ArcShape CircleSector(float radius, float angle, float rotation = 0.0f, int segmentSlices = 32, int loopSlices = 0) {
      return DiscSector(0.0f, radius, angle, rotation, segmentSlices, loopSlices);
    }

    public static ArcShape Disc(float innerRadius, float outerRadius, int segmentSlices = 32, int loopSlices = 0) {
      return DiscSector(innerRadius, outerRadius, 2.0f * (float)math.PI, 0.0f, segmentSlices, loopSlices);
    }

    public static ArcShape DiscSector(float innerRadius, float outerRadius, float angle, float rotation = 0.0f, int segmentSlices = 32, int loopSlices = 0) {
      return new ArcShape { radius = math.float2(innerRadius, outerRadius), angle = math.float2(rotation - 0.5f * angle, rotation + 0.5f * angle), slices = math.int2(segmentSlices, loopSlices) };
    }

    public static ArcShape.Path Path(this in ArcShape arc) => new ArcShape.Path { arc = arc };


    public static SphereShape Sphere(float radius, int segmentSlices = 32, int loopSlices = 32) {
      return new SphereShape { radius = radius, slices = math.int2(segmentSlices, loopSlices) };
    }

  }
}
