using Unity.Mathematics;

namespace Monoid.Unity.Graphics {

  public struct ArcShape : IShape {

    public struct Path : IShape {
      public ArcShape arc;

      public int Vertices => arc.Vertices;
      public int Quads => arc.Quads;
      public int Triangles => arc.Triangles;

      public int4 Quad(int index) => arc.Quad(index);
      public int3 Triangle(int index) => arc.Triangle(index);

      public float3 Position(int index) => arc.Position(index);
      public float3 Normal(int index) => arc.Normal(index);
      public float2 Texture(int index) => arc.Params(index);
    }


    public float2 radius;
    public float2 angle;
    public int2 slices;

    public int Vertices => (slices.x + 2) * (slices.y + 2);
    public int Quads => (slices.x + 1) * (slices.y + 1);
    public int Triangles => 2 * Quads;

    public int4 Quad(int index) {
      var n = slices.x + 1;
      var i = math.int4(index % n, index / n, 0, 1);
      return (n + 1) * (i.yyyy + i.zzww) + (i.xxxx + i.zwzw);
    }

    public int3 Triangle(int index) {
      var quad = Quad(index / 2);
      return index % 2 == 0 ? quad.xyz : quad.wzy;
    }

    public float3 Position(int index) {
      return math.float3(radius.y * Coord(index), 0);
    }

    public float3 Normal(int index) {
      return math.float3(0, 0, 1);
    }

    public float2 Texture(int index) => 0.5f + 0.5f * Coord(index);

    public float2 Texture2(int index) => Params(index);

    float2 Coord(int index) {
      var p = Params(index);
      var r = math.lerp(radius.x, radius.y, p.y);
      if (radius.y != 0) {
        r /= radius.y;
      }
      float2 cs;
      math.sincos(math.lerp(angle.x, angle.y, p.x), out cs.y, out cs.x);
      return (r * cs);
    }

    float2 Params(int index) {
      var n = slices.x + 2;
      var i = math.float2(index % n, index / n);
      var p = i / (slices + 1);
      return p;
    }
  }
}
