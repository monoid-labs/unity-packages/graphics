﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace Monoid.Unity.Graphics {
  [RequireComponent(typeof(Renderer))]
  public class PerRendererProperties : MonoBehaviour {

    #region Property

    [Serializable]
    public struct Property {
      public enum Type {
        Color,
        Vector,
        Float,
        Range,
        Texture
      }

#pragma warning disable 0649

      public string name, label;
      public Type type;
      public Color color;
      public Vector4 vector;
      public Texture texture;
      public bool scaleOffset;

#pragma warning restore 0649

      public void Set(MaterialPropertyBlock mpb) {
        switch (type) {
          case Type.Color:
            mpb.SetColor(name, color);
            break;
          case Type.Float:
          case Type.Range:
            mpb.SetFloat(name, vector.x);
            break;
          case Type.Vector:
            mpb.SetVector(name, vector);
            break;
          case Type.Texture:
            if (texture) {
              mpb.SetTexture(name, texture);
            }
            if (scaleOffset) {
              mpb.SetVector(name + "_ST", vector);
            }
            break;
        }
      }
    }

    #endregion


    static MaterialPropertyBlock mpb; // static because MonoBehaviors run on the Unity Main Thread

    public Property[] properties = { };

    public void Set() {
      var renderer = GetComponent<Renderer>();
      if (renderer) {
        if (mpb == null) {
          mpb = new MaterialPropertyBlock();
        }
        mpb.Clear();
        renderer.GetPropertyBlock(mpb);
        foreach (var prop in properties) {
          prop.Set(mpb);
        }
        renderer.SetPropertyBlock(mpb);
      }
    }

    void Awake() {
      Set();
#if !UNITY_EDITOR
      Destroy (this);
#endif
    }
  }

}