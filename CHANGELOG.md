# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.6] - 2021-01-20

- Unity 2020.2 support (`UnityEditor.Experimental.AssetImporters` -> `UnityEditor.AssetImporters`)
- Unity.Mathematics dependency (proper)

## [1.0.5] - 2021-01-20

- Texture Names

## [1.0.4] - 2020-12-12

- Text Mesh Generation (without a GameObject)

## [1.0.3] - 2020-12-10

- Shapes clean-up

## [1.0.2] - 2020-11-27

- Texture3D & Texture2DArray assset importers

## [1.0.1] - 2020-11-25

- Arc.Path

## [1.0.0-preview] - 2020-06-28

- BSD 2-Clause License
- `.editorconfig`
- Combined material/mesh/texture packages
