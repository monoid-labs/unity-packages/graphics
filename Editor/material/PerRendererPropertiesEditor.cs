﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Monoid.Unity.Graphics {

  using Property = PerRendererProperties.Property;

  [CustomEditor(typeof(PerRendererProperties))]
  public class PerRendererPropertiesEditor : Editor {

    public override void OnInspectorGUI() {
      DrawPropertiesExcluding(serializedObject, "properties");

      var target = this.target as PerRendererProperties;
      if (!target) {
        return;
      }

      var dirty = false;

      if (GUILayout.Button("Sync PerRendererData Properties")) {
        Sync(target);
        dirty = true;
      }

      EditorGUILayout.Separator();

      EditorGUI.BeginChangeCheck();
      var properties = target.properties;
      for (int i = 0, n = properties.Length; i < n; i++) {
        var prop = properties[i];
        switch (prop.type) {
          case PerRendererProperties.Property.Type.Color:
            prop.color = EditorGUILayout.ColorField(prop.label, prop.color);
            break;
          case PerRendererProperties.Property.Type.Vector:
            prop.vector = EditorGUILayout.Vector4Field(prop.label, prop.vector);
            break;
          case PerRendererProperties.Property.Type.Float:
            prop.vector.x = EditorGUILayout.FloatField(prop.label, prop.vector.x);
            break;
          case PerRendererProperties.Property.Type.Range:
            prop.vector.x = EditorGUILayout.Slider(prop.label, prop.vector.x, prop.vector.y, prop.vector.z);
            break;
          case PerRendererProperties.Property.Type.Texture:
            prop.texture = (Texture)EditorGUILayout.ObjectField(prop.label, prop.texture, typeof(Texture), false);
            if (prop.scaleOffset) {
              EditorGUI.indentLevel++;
              prop.vector = EditorGUILayout.Vector4Field("Scale and Offset", prop.vector);
              EditorGUI.indentLevel--;
            }
            break;
        }
        properties[i] = prop;
      }

      dirty |= EditorGUI.EndChangeCheck();
      if (dirty) {
        SetDirty(target);
      }
    }


    void Sync(PerRendererProperties target) {
      var renderer = target.GetComponent<Renderer>();
      if (!renderer) {
        return;
      }

      var mats = renderer.sharedMaterials;
      if (mats.Length == 0) {
        target.properties = new Property[0];
      }

      var list = new List<Property>();
      var props = MaterialEditor.GetMaterialProperties(mats);
      foreach (var prop in props) {
        if (!prop.flags.HasFlag(MaterialProperty.PropFlags.PerRendererData)) {
          continue;
        }

        var property = new Property {
          name = prop.name,
          label = prop.displayName,
        };
        switch (prop.type) {
          case MaterialProperty.PropType.Texture:
            property.type = Property.Type.Texture;
            property.vector = prop.textureScaleAndOffset;
            property.scaleOffset = !prop.flags.HasFlag(MaterialProperty.PropFlags.NoScaleOffset);
            break;
          case MaterialProperty.PropType.Color:
            property.type = Property.Type.Color;
            property.color = prop.colorValue;
            break;
          case MaterialProperty.PropType.Vector:
            property.type = Property.Type.Vector;
            property.vector = prop.vectorValue;
            break;
          case MaterialProperty.PropType.Range:
            property.type = Property.Type.Range;
            property.vector.x = prop.floatValue;
            property.vector.y = prop.rangeLimits.x;
            property.vector.z = prop.rangeLimits.y;
            break;
          case MaterialProperty.PropType.Float:
            property.type = Property.Type.Float;
            property.vector.x = prop.floatValue;
            break;
        }

        list.Add(property);
      }
      target.properties = list.ToArray();
      SetDirty(target);
    }

    void SetDirty(PerRendererProperties target) {
      target.Set();
      if (EditorApplication.isPlayingOrWillChangePlaymode) {
        return;
      }
      EditorUtility.SetDirty(this);
      if (target.gameObject.scene.isLoaded) {
        EditorSceneManager.MarkSceneDirty(target.gameObject.scene);
      }
    }

  }

}