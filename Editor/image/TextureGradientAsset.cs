using System.IO;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Graphics {

  [ScriptedImporter(1, "gradtex")]
  public sealed class TextureGradientAsset : ScriptedImporter {

    #region Create Asset in Project View

    [MenuItem("Assets/Create/Monoid/TextureGradient", priority = 9000)]
    public static void Create() {
      var folder = AssetDatabase.GetAssetPath(Selection.activeObject);
      var name = "gradient";

      File.WriteAllText(Path.Combine(folder, name + ".gradtex"), string.Empty);
    }

    [MenuItem("Assets/Create/Monoid/TextureGradient", true, priority = 9000)]
    public static bool CanCreate() {
      var folder = AssetDatabase.GetAssetPath(Selection.activeObject);
      return AssetDatabase.IsValidFolder(folder);
    }

    #endregion


    public Vector2Int size = new Vector2Int(16, 1);
    public Gradient gradient = new Gradient();
    public bool alphaIsTransparency = false;
    public TextureWrapMode wrapMode = TextureWrapMode.Clamp;
    public FilterMode filterMode = FilterMode.Bilinear;
    public bool generateMipMaps = false;
    public bool compress = false;
    public bool readWriteEnabled = true;

    public override void OnImportAsset(AssetImportContext ctx) {
      var path = ctx.assetPath;
      var name = Path.GetFileName(path);

      compress &= (size.x % 4 == 0) && (size.y % 4 == 0);

      var format = Gradients.IsOpaque(gradient) ? TextureFormat.RGB24 : TextureFormat.RGBA32;
      var texture = new Texture2D(size.x, size.y, format, generateMipMaps) {
        name = name,
        alphaIsTransparency = alphaIsTransparency,
      };
      Gradients.Sample(gradient, texture, generateMipMaps);
      if (compress) {
        texture.Compress(true);
      }
      texture.filterMode = filterMode;
      texture.wrapMode = wrapMode;
      if (!readWriteEnabled) {
        texture.Apply(false, true);
      }

      ctx.AddObjectToAsset(name, texture);
      ctx.SetMainObject(texture);
    }
  }

}
