using System.IO;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Graphics {

  [CustomEditor(typeof(TextureGradientAsset))]
  public sealed class TextureGradientAssetEditor : ScriptedImporterEditor {

    public override void OnInspectorGUI() {
      serializedObject.Update();
      DrawPropertiesExcluding(serializedObject);
      serializedObject.ApplyModifiedProperties();

      ApplyRevertGUI();

      EditorGUILayout.Separator();

      if (GUILayout.Button("Export")) {
        var t = (TextureGradientAsset)target;
        var path = t.assetPath;
        var dir = Path.GetDirectoryName(path);
        var file = Path.GetFileNameWithoutExtension(path);
        var save = EditorUtility.SaveFilePanel("Export Gradient Texture", dir, file, "png");
        if (save.Length > 0) {
          var name = Path.GetFileName(save);
          var format = Gradients.IsOpaque(t.gradient) ? TextureFormat.RGB24 : TextureFormat.RGBA32;
          var texture = new Texture2D(t.size.x, t.size.y, format, false) {
            name = name,
          };
          Gradients.Sample(t.gradient, texture);
          File.WriteAllBytes(save, texture.EncodeToPNG());
        }
      }
    }
  }

}
