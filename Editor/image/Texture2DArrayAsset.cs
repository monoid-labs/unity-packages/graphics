using System;
using System.IO;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Graphics {

  [ScriptedImporter(1, "tex2d-array")]
  public sealed class Texture2DArrayAsset : ScriptedImporter {

    #region Create Asset in Project View

    [MenuItem("Assets/Create/Monoid/Texture2DArray", priority = 9000)]
    public static void Create() {
      var folder = AssetDatabase.GetAssetPath(Selection.activeObject);
      var name = "slices";

      File.WriteAllText(Path.Combine(folder, name + ".tex2d-array"), string.Empty);
    }

    [MenuItem("Assets/Create/Monoid/Texture2DArray", true, priority = 9000)]
    public static bool CanCreate() {
      var folder = AssetDatabase.GetAssetPath(Selection.activeObject);
      return AssetDatabase.IsValidFolder(folder);
    }

    #endregion

    public Texture2D[] array = { };

    public TextureFormat format = TextureFormat.RGBA32;
    public bool linear = true;
    public TextureWrapMode wrapMode = TextureWrapMode.Clamp;
    public FilterMode filterMode = FilterMode.Bilinear;
    public bool generateMipMaps = false;
    public bool compress = false;
    public bool readWriteEnabled = true;

    public override void OnImportAsset(AssetImportContext ctx) {
      if (array.Length == 0) {
        return;
      }

      var path = ctx.assetPath;
      var name = Path.GetFileName(path);

      int width = 0, height = 0, depth = array.Length;
      for (int i = 0; i < depth; i++) {
        var slice = array[i];
        if (!slice) {
          continue;
        }
        width = Math.Max(width, slice.width);
        height = Math.Max(height, slice.height);
      }

      var texture = new Texture2DArray(width, height, depth, format, generateMipMaps, linear) {
        name = name,
      };

      for (int z = 0; z < depth; z++) {
        var colors = new Color[width * height];

        var slice = array[z];
        if (!slice) {
          texture.SetPixels(colors, z, 0);
          continue;
        }

        var src = slice.GetPixels(0);
        int w = slice.width, h = slice.height;

        for (int y = 0; y < h; y++) {
          Array.Copy(src, y * w, colors, y * width, w);
        }
        texture.SetPixels(colors, z, 0);
      }

      texture.Apply(generateMipMaps, !readWriteEnabled);



      ctx.AddObjectToAsset(name, texture);
      ctx.SetMainObject(texture);

      for (int i = 0; i < depth; i++) {
        var assetPath = AssetDatabase.GetAssetPath(array[i]);
        if (assetPath.Length > 0) {
          ctx.DependsOnSourceAsset(assetPath);
        }
      }
    }
  }


}
